"""Parser module."""

import logging
from pathlib import Path

log = logging.getLogger()


def parse_config(context):
    """Parses config.json."""
    file_type = context.get_input("input-file")["object"]["type"]
    file_path = context.get_input("input-file")["location"]["path"]
    tag = context.config.get("tag")
    metadata_location = context.config.get("metadata_location", "")
    # Just not 100% sure if the config will populate this with "None"
    # if it's not provided or not.
    if metadata_location is None:
        metadata_location = ""

    return file_path, file_type, tag, metadata_location


def find_file_type(file_path: Path, type_dict: dict) -> str:
    file_type = None
    name = str(file_path)
    for ft, suffixes in type_dict.items():
        if any([name.endswith(suffix) for suffix in suffixes]):
            file_type = ft
            break
    if file_type is None:
        log.warning("Could not determine file type from suffix.")

    return file_type
