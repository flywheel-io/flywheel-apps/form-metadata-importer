"""Tests for fw_file_json class."""

import json as pyjson
from pathlib import Path

import pytest
from dotty_dict import dotty

from fw_gear_form_importer.files.fw_file_json import JSON

INVALID_JSON = "invalid_json.json"
VALID_JSON = "test_json.json"

VALID_JSON_DATA = dotty(
    {
        "string_key": "string",
        "num_key": 123,
        "zero_key_int": 0,
        "zero_key_float": 0.0,
        "keep_bool": False,
        "top_level_empty": "",
        "empty_key_remove": {"empty_key_nest": ""},
        "nested_key_parent": {"nested_key1": {"nested_key2": "nested2_val"}},
    }
)

VALID_JSON_KEYS = [
    "nested_key_parent.nested_key1.nested_key2",
    "empty_key_remove.empty_key_nest",
    "nested_key_parent.nested_key1",
    "string_key",
    "num_key",
    "zero_key_int",
    "zero_key_float",
    "keep_bool",
    "top_level_empty",
    "empty_key_remove",
    "nested_key_parent",
]

VALID_JSON_KEYS_REVERSED = [
    "string_key",
    "num_key",
    "zero_key_int",
    "zero_key_float",
    "keep_bool",
    "top_level_empty",
    "empty_key_remove",
    "nested_key_parent",
    "empty_key_remove.empty_key_nest",
    "nested_key_parent.nested_key1",
    "nested_key_parent.nested_key1.nested_key2",
]


VALID_JSON_NOBLANKS = dotty(
    {
        "string_key": "string",
        "num_key": 123,
        "nested_key_parent": {"nested_key1": {"nested_key2": "nested2_val"}},
        "zero_key_int": 0,
        "zero_key_float": 0.0,
        "keep_bool": False,
    }
)


def test_json_init_from_invalid_file(tmp_path):
    invalid_json_file = tmp_path / "invalid.json"
    json_str = (
        '{\n    \'key1\': {\n        "key2": "val2",\n        "key3": "val3"\n    }\n}'
    )
    with open(invalid_json_file, "w") as text_file:
        text_file.write(json_str)

    with pytest.raises(pyjson.decoder.JSONDecodeError):
        JSON(invalid_json_file)


def test_json_init_from_file(json_file):
    fw_json = JSON(json_file(VALID_JSON))
    assert fw_json.fields == VALID_JSON_DATA


def test_json_save(json_file, tmp_path):
    path = tmp_path / "destination.json"
    json_instance = JSON(json_file(VALID_JSON))
    json_instance.save(path)
    assert path.exists()


def test_json_meta(json_file):
    json_instance = JSON(json_file(VALID_JSON))

    assert json_instance.get_meta() == {
        "file.type": "json",
        "file.name": Path(json_instance.localpath).name,
    }


def test_json_get(json_file):
    json_instance = JSON(json_file(VALID_JSON))

    assert json_instance.get("string_key") == "string"


def test_json_set(json_file):
    json_instance = JSON(json_file(VALID_JSON))

    json_instance["magic"] = b"n+2"
    assert json_instance.get("magic") == b"n+2"


def test_json_del(json_file):
    json_instance = JSON(json_file(VALID_JSON))

    del json_instance["num_key"]
    assert "num_key" not in json_instance.fields


def test_json_len(json_file):
    json_instance = JSON(json_file(VALID_JSON))

    assert len(json_instance) == len(VALID_JSON_DATA)


def test_json_iter(json_file):
    json_instance = JSON(json_file(VALID_JSON))

    assert len(list(json_instance)) == len(VALID_JSON_DATA)


def test_get_all_keys_unsorted(json_file):
    json_instance = JSON(json_file(VALID_JSON))

    sorted_keys = json_instance.get_all_keys(sort=False, reverse=False)
    assert all([i in sorted_keys for i in VALID_JSON_KEYS])
    assert all([i in VALID_JSON_KEYS for i in sorted_keys])
    assert sorted_keys != VALID_JSON_KEYS


def test_get_all_keys_sorted_deepest_first(json_file):
    json_instance = JSON(json_file(VALID_JSON))

    sorted_keys = json_instance.get_all_keys(sort=True, reverse=True)
    assert sorted_keys == VALID_JSON_KEYS


def test_get_all_keys_sorted_deepest_last(json_file):
    json_instance = JSON(json_file(VALID_JSON))

    sorted_keys = json_instance.get_all_keys(sort=True, reverse=False)
    assert sorted_keys == VALID_JSON_KEYS_REVERSED


def test_to_dict(json_file):
    json_instance = JSON(json_file(VALID_JSON))

    assert json_instance.to_dict() == VALID_JSON_DATA


def test_remove_blanks_from_data(json_file):
    json_instance = JSON(json_file(VALID_JSON))

    json_instance.remove_blanks()
    assert json_instance.fields == VALID_JSON_NOBLANKS
