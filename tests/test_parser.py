from pathlib import Path

from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_form_importer.parser import find_file_type, parse_config
from fw_gear_form_importer.util import FILETYPES
from tests.conftest import ASSETS_ROOT


def test_parse_config():
    context = GearToolkitContext(config_path=ASSETS_ROOT / "config.json", input_args=[])
    file_path, file_type, tag, metadata_location = parse_config(context)
    assert file_path == "/some/path/tmp.json"
    assert file_type == "source code"
    assert tag is None
    assert metadata_location == "info.forms.json"


def test_find_file_type():
    fname = Path("/some/path/tmp.json")
    ftype = find_file_type(fname, FILETYPES)
    assert ftype == "source code"
