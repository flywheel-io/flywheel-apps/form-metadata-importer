import json
from pathlib import Path

import flywheel
import pytest

from fw_gear_form_importer.files import json_file as fw_json
from fw_gear_form_importer.main import project_tag_update, run


def test_project_tag_update():
    project_info = {
        "context": {
            "forms": {
                "json": {
                    "allow_key1": True,
                    "allow_key2": True,
                    "deny_key1": False,
                    "deny_key2": False,
                }
            }
        }
    }
    project = flywheel.Project(label="Test", info=project_info)
    project_tag_update(project)
    assert fw_json.DENY_KEYS == {"deny_key1", "deny_key2"}
    assert fw_json.ALLOW_KEYS == {"allow_key1", "allow_key2"}


def test_run_invalid_filetype():
    file_type = None
    project = flywheel.Project(label="Test", info={})
    file_path = Path("/some/path/unknown_file.xyz")
    with pytest.raises(SystemExit):
        fe, meta, qc = run(file_type, file_path, project)


def test_run(json_file):
    fw_json.DENY_KEYS = {}
    fw_json.ALLOW_KEYS = {}

    fname = "test_json.json"
    json_path = json_file(fname)
    file_type = None
    project = flywheel.Project(label="Test", info={})
    fe, meta, qc = run(
        file_type, json_path, project, metadata_location="info.forms.json"
    )

    validation_json = "test_json_removed_blanks.json"
    json_path = json_file(validation_json)
    with open(json_path, "r") as json_file:
        json_data = json.load(json_file)

    assert fe["info"]["forms"]["json"] == json_data
    assert qc == {}
    assert meta == {"file.type": "json", "file.name": "test_json.json"}
