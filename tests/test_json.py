import logging

import pytest
from dotty_dict import dotty

from fw_gear_form_importer.files import json_file as jf
from fw_gear_form_importer.files.json_file import (
    generate_file_entry,
    get_fw_filtered_fields,
    process,
    update_array_keys,
)

logging.basicConfig(level="DEBUG")
log = logging.getLogger()

jf.ALLOW_KEYS = set()
jf.DENY_KEYS = set()


def test_process(json_file, json_data):
    fname = "test_json.json"
    json_path = json_file(fname)
    fe, meta, qc = process(
        json_path, remove_blanks=False, metadata_location="info.forms.json"
    )

    assert fe["info"]["forms"]["json"] == json_data.to_dict()
    assert qc == {}
    assert meta == {"file.type": "json", "file.name": "test_json.json"}


def test_get_fw_filtered_fields_allow(json_instance):
    jf.DENY_KEYS = set()
    jf.ALLOW_KEYS = {"string_key"}

    fw_json = json_instance
    get_fw_filtered_fields(fw_json)

    assert fw_json.fields == dotty({"string_key": "string"})


def test_get_fw_filtered_fields_deny(json_instance):
    jf.ALLOW_KEYS = set()
    jf.DENY_KEYS = {"string_key"}

    fw_json = json_instance
    get_fw_filtered_fields(fw_json)

    assert "string_key" not in fw_json.fields


def test_update_array_tag():
    jf.ALLOW_KEYS = set()
    jf.DENY_KEYS = set()
    valid_custom_tags = {
        "valid_bool_pass": True,
        "valid_str_pass": "tRUE",
        "valid_bool_deny": False,
        "valid_str_deny": "fALSE",
    }
    invalid_custom_tags = {"invalid_str_pass": "pass", "invalid_str_deny": "block"}
    update_array_keys(valid_custom_tags)

    assert jf.ALLOW_KEYS == {"valid_bool_pass", "valid_str_pass"}
    assert jf.DENY_KEYS == {"valid_bool_deny", "valid_str_deny"}

    with pytest.raises(SystemExit):
        update_array_keys(invalid_custom_tags)
    jf.ALLOW_KEYS = set()
    jf.DENY_KEYS = set()


def test_remove_blanks_from_data(json_instance):
    fw_json = json_instance
    fw_json.remove_blanks()
    assert "empty_key_remove" not in fw_json.fields
    assert "zero_key_int" in fw_json.fields
    assert "zero_key_float" in fw_json.fields
    assert "top_level_empty" not in fw_json.fields
    assert "keep_bool" in fw_json.fields


def test_sort_keys_by_depth(json_instance, json_data_keys):
    sorted_keys = json_instance.get_all_keys(sort=True, reverse=True)
    assert sorted_keys == json_data_keys


def test_generate_file_entry_top_level():
    file_dict = {"key1": "value1", "key2": "value2"}
    metadata_location = "info"
    expected_output = {"modality": "Form", "info": {"key1": "value1", "key2": "value2"}}
    assert generate_file_entry(file_dict, metadata_location) == expected_output


def test_generate_file_entry_nested():
    file_dict = {"key1": "value1", "key2": "value2"}
    metadata_location = "info.nested"
    expected_output = {
        "modality": "Form",
        "info": {"nested": {"key1": "value1", "key2": "value2"}},
    }
    assert generate_file_entry(file_dict, metadata_location) == expected_output


def test_generate_file_entry_deeply_nested():
    file_dict = {"key1": "value1", "key2": "value2"}
    metadata_location = "info.nested.deeply"
    expected_output = {
        "modality": "Form",
        "info": {"nested": {"deeply": {"key1": "value1", "key2": "value2"}}},
    }
    assert generate_file_entry(file_dict, metadata_location) == expected_output


def test_generate_file_entry_empty_metadata_location():
    file_dict = {"key1": "value1", "key2": "value2"}
    metadata_location = ""
    expected_output = {"modality": "Form", "info": {"key1": "value1", "key2": "value2"}}
    assert generate_file_entry(file_dict, metadata_location) == expected_output


def test_generate_file_entry_no_metadata_location():
    file_dict = {"key1": "value1", "key2": "value2"}
    metadata_location = None
    expected_output = {"modality": "Form", "info": {"key1": "value1", "key2": "value2"}}
    assert generate_file_entry(file_dict, metadata_location) == expected_output
