import json
from pathlib import Path
from unittest import mock

import flywheel
from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_form_importer.main import run
from fw_gear_form_importer.util import (
    create_metadata,
    get_startswith_lstrip_dict,
    sanitize_modality,
    validate_metadata_location,
)
from tests.conftest import ASSETS_ROOT


def test_get_startswith_dict_return_filtered_dict():
    dict_ = {"bla.toto": None, "bli.titi": None}
    res = get_startswith_lstrip_dict(dict_, "bla")
    assert res == {"toto": None}


def test_create_metadata(tmpdir, json_file):
    with GearToolkitContext(
        gear_path=tmpdir,
        config_path=ASSETS_ROOT / "config.json",
        input_args=[],
        manifest_path=ASSETS_ROOT.parents[1] / "manifest.json",
    ) as context:
        context.get_destination_container = mock.MagicMock()
        fname = "test_json.json"
        json_path = json_file(fname)
        file_type = None
        project = flywheel.Project(label="Test", info={})
        fe, meta, qc = run(
            file_type, json_path, project, metadata_location="info.forms.json"
        )

        create_metadata(context, fe, meta, qc)

    with open(Path(tmpdir) / "output" / ".metadata.json", "r") as fp:
        metadata = json.load(fp)
    fname = "test_json_removed_blanks.json"
    json_path = json_file(fname)
    with open(json_path, "r") as json_file:
        json_data = json.load(json_file)

    info = metadata["acquisition"]["files"][0]["info"]
    assert info["forms"] == {"json": json_data}


def test_sanitize_modality():
    res = sanitize_modality("PET/CT")
    assert res == "PET-CT"


@mock.patch("fw_gear_form_importer.util.log")
def test_validate_metadata_location_none(mock_log):
    assert not validate_metadata_location(None)
    mock_log.error.assert_called_with("metadata_location is blank.")


@mock.patch("fw_gear_form_importer.util.log")
def test_validate_metadata_location_invalid_start(mock_log):
    assert not validate_metadata_location("data.info")
    mock_log.error.assert_called_with(
        "metadata keys must start with 'info.' or be just 'info'"
    )


@mock.patch("fw_gear_form_importer.util.log")
def test_validate_metadata_location_invalid_chars(mock_log):
    assert not validate_metadata_location("info._data")
    mock_log.error.assert_called_with(
        "metadata keys cannot start with " + str(["_", ".", "$"])
    )


def test_validate_metadata_location_valid():
    assert validate_metadata_location("info.data")
    assert validate_metadata_location("info")
