import functools
import shutil
import tempfile
from pathlib import Path

import pytest
from dotty_dict import dotty

from fw_gear_form_importer.files.fw_file_json import JSON

ASSETS_ROOT = Path(__file__).parent / "assets"
JSON_ROOT = ASSETS_ROOT / "JSON"
DICOM_ROOT = ASSETS_ROOT / "DICOM"
PARAVISION_ROOT = ASSETS_ROOT / "PARAVISION"
PARREC_ROOT = ASSETS_ROOT / "PARREC"


def get_asset_file(root_dir, filename):
    """Returns path to file in assets."""
    if not isinstance(filename, Path):
        filename = Path(filename)
    path = Path(tempfile.mkdtemp())
    shutil.copy(root_dir / filename, path / filename)
    return str(path / filename)


@pytest.fixture
def json_file():
    get_asset = functools.partial(get_asset_file, JSON_ROOT)
    return get_asset


@pytest.fixture
def invalid_json_path(tmp_path, json_file):
    fname = "invalid_json.json"
    json_path = json_file(fname)
    return json_path


@pytest.fixture
def json_data():
    data = {
        "string_key": "string",
        "num_key": 123,
        "zero_key_int": 0,
        "zero_key_float": 0.0,
        "keep_bool": False,
        "top_level_empty": "",
        "empty_key_remove": {"empty_key_nest": ""},
        "nested_key_parent": {"nested_key1": {"nested_key2": "nested2_val"}},
    }
    return dotty(data)


@pytest.fixture
def json_data_keys():
    return [
        "nested_key_parent.nested_key1.nested_key2",
        "empty_key_remove.empty_key_nest",
        "nested_key_parent.nested_key1",
        "string_key",
        "num_key",
        "zero_key_int",
        "zero_key_float",
        "keep_bool",
        "top_level_empty",
        "empty_key_remove",
        "nested_key_parent",
    ]


@pytest.fixture
def json_instance(json_file):
    j = JSON(json_file("test_json.json"))
    return j
